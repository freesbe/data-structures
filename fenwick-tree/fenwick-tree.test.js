import FenwickTree from "./fenwickTree";

describe("Fenwick Tree", () => {
  const tree = FenwickTree([3, 7, -2, 12, 3, 14, 5, 23, -9, 2, 4, -8]);

  const cases = [
    [1, 7, 42],
    [2, 3, 5],
    [1, 2, 10],
    [9, 11, -3],
    [11, 12, -4],
  ];

  test.each(cases)(
    "given range of [%p, %p] returns sum equal to %p",
    (i, j, expected) => {
      expect(tree.sum(i, j)).toEqual(expected);
    }
  );

  test("adds 5 to first item", () => {
    expect(tree.sum(1, 2)).toEqual(10);

    tree.add(1, 5);

    expect(tree.sum(1, 2)).toEqual(15);
  });

  test("throws an error when upper bound index is lower then lower bound index", () => {
    expect(() => tree.sum(10, 5)).toThrowError("Make sure j > i");
  });
});

test.each([[], null, undefined])(
  "throws an error when no values provided",
  (values) => {
    expect(() => FenwickTree(values)).toThrowError(
      "Values array cannot be empty"
    );
  }
);
