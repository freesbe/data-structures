import { lsb } from "../binaries/lsb";

/**
 * Prefixes sum of elements [1, i], one based.
 *
 * @param {*} tree Fenwick Tree
 * @param {*} i index
 */
function prefixSum(tree, i) {
  let sum = 0;

  while (i != 0) {
    sum += tree[i];

    i -= lsb(i);
  }

  return sum;
}

/**
 * Computes sum of interval [i, j], one based.
 *
 * @param {*} tree Fenwick Tree
 * @param {*} i lower bound index
 * @param {*} j upper bound index
 */
function sum(tree, i, j) {
  if (i > j) {
    throw new Error("Make sure j > i");
  }

  return prefixSum(tree, j) - prefixSum(tree, i - 1);
}

/**
 * Adds k to element at i, one based.
 * 
 * @param {*} tree Fenwick Tree
 * @param {*} i index
 * @param {*} k value to add
 */
function add(tree, i, k) {
  while (i < tree.length) {
      tree[i] += k;

      i += lsb(i);
  }
}

/**
 * Constructs Fenwick Tree.
 * Since zero has no least significant bit we make Fenwick trees one based instead of zero based.
 *
 * @param {*} values array of values
 */
function construct(values) {
  if (!values || values.length === 0) {
    throw new Error("Values array cannot be empty");
  }

  const tree = [NaN, ...values]; // Remember, one based

  for (let i = 1; i < tree.length; i++) {
    const j = i + lsb(i);

    if (j < tree.length) {
      tree[j] += tree[i];
    }
  }

  return tree;
}

export default function (values) {
  const tree = construct(values);

  return {
    sum: function (i, j) {
      return sum(tree, i, j);
    },
    add: function (i, k) {
      return add(tree, i, k);
    },
  };
}
