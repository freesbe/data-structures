import { lsb } from "./lsb";

const cases = [
  [1, 1],
  [2, 2],
  [3, 1],
  [4, 4],
  [108, 4],
  [104, 8],
  [96, 32],
  [64, 64],
];

test.each(cases)(
  "given %p returns %p as least significant bit",
  (i, expected) => {
    expect(lsb(i)).toEqual(expected);
  }
);
