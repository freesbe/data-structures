function insert(root) {
  return function (value) {
    // TODO
  };
}

function remove(root) {
  return function (value) {
    // TODO
  };
}

function contains(root) {
  return function (value) {
    // TODO
  };
}

function create(values) {
  const root = {
    left: {}, // TODO
    right: {}, // TODO
  };

  return root;
}

export default function (values) {
  const root = create(values);

  return {
    insert: insert(root),
    remove: remove(root),
  };
}
